import sys
import os
from PyQt5 import QtWidgets

sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Controller'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Views'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Lib'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Files'))
from Main_models_infor import Main_model_infor
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_login import Ui_login

class Main_login():
    def __init__(self):
        self.dialog = QDialog()
        self.uic = Ui_login()
        self.uic.setupUi(self.dialog)
        self.dialog.closeEvent=self.closeEvent
        self.logctrl=ControlerClass()
        self.event()
    def login(self):
        user=self.uic.lineEdit_user.text()
        password = self.uic.lineEdit_password.text()
        # user_get,pas_get= self.logctrl.get_users()
        # if user==user_get and password==pas_get:
        if user=="a" and password=="123":
            self.show_model_infor()
        #   go to view models
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Critical)
            msg.setText("Username  or password is not valid!")
            msg.setWindowTitle("Error")
            msg.exec_()
    def event(self):
        self.uic.btnLogin.clicked.connect(self.login)
    def closeEvent(self, event):
            close = QMessageBox()
            close.setWindowTitle("Information")
            close.setText( "Are you sure want to exit?")
            close.setStandardButtons(QMessageBox.No|QMessageBox.Yes )
            close = close.exec()
            if close == QMessageBox.Yes:
                    event.accept()
            else:
                    event.ignore()
    def show(self):
        self.dialog.exec()
    def show_model_infor(self):
            a=Main_model_infor()
            a.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = Main_login()
    dialog.show()
    sys.exit(app.exec())
  