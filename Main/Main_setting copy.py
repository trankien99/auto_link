import sys
import os
from PyQt5 import QtGui
from PyQt5 import QtWidgets

sys.path.insert(0,os.path.realpath('./Controller'))
sys.path.insert(0,os.path.realpath('./Views'))
sys.path.insert(0,os.path.realpath('./Lib'))
sys.path.insert(0,os.path.realpath('./Files'))

from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QPixmap
import cv2   
from HikCamera import KeyName,HikCamera
from parameter import parameter
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_modelInfor import Ui_ModelManager
from UI_models_setting import Ui_setting_model
import threading 

class Main_model_setting():
    def __init__(self,modelname):
        self.modelname=modelname
        self.main_win = QDialog()
        self.uic = Ui_setting_model()
        self.uic.setupUi(self.main_win)
        self.main_win.closeEvent=self.closeEvent
        self.z_value_go=0
        self.y_value_go=0
        self.stop_camera=True
        self.captureimage=False
        self.path_to_imgage=''
        self.listcode=[]
        self.model_crtl=ControlerClass()
        self.load_model_setting()
        self.plc=self.model_crtl.connect_to_PLC()
        self.light=self.model_crtl.connect_to_light()
        self.camera=self.model_crtl.connect_to_camera()
        self.event()
        self.login_plc()
    def event(self):
        self.uic.btnsave.clicked.connect(self.save_File)
        # self.uic.btnExit.clicked.connect(self.btnExitclicked)
        self.uic.btnread.clicked.connect(self.btnReadClicked)

        self.uic.sldExposuretime.setValue(int(self.uic.leExposureTime.text()))
        self.uic.sldGamma.setValue(int(self.uic.leGama.text()))
        self.uic.sldCH1.setValue(int(self.uic.leCH1.text()))
        self.uic.sldCH2.setValue(int(self.uic.leCH2.text()))
       
        self.uic.sldExposuretime.valueChanged.connect(lambda:self.sldExposuretime_valueChanged())
        self.uic.sldGamma.valueChanged.connect(lambda:self.sldGamma_valueChanged())
        self.uic.sldCH1.valueChanged.connect(lambda:self.uic.leCH1.setText(str(self.uic.sldCH1.value())))
        self.uic.sldCH2.valueChanged.connect(lambda:self.uic.leCH2.setText(str(self.uic.sldCH2.value())))

        self.uic.leExposureTime.editingFinished.connect(lambda:self.leExposuretime_edit())
        self.uic.leGama.editingFinished.connect(lambda:self.uic.sldGamma.setValue(float(self.uic.leGama.text())))
        self.uic.leCH1.editingFinished.connect(lambda:self.uic.sldCH1.setValue(int(self.uic.leCH1.text())))
        self.uic.leCH2.editingFinished.connect(lambda:self.uic.sldCH2.setValue(float(self.uic.leCH2.text())))
        #light and camera 
        if self.light!=-1:
            self.light.Turn_On_Light12()
            self.uic.leCH1.textChanged.connect(lambda:self.setlight1())
            self.uic.leCH2.textChanged.connect(lambda:self.setlight2())
        if self.camera!=-1:
            self.uic.leExposureTime.textChanged.connect(lambda:self.setExposureTime())
            self.uic.leGama.textChanged.connect(lambda:self.setGamma())
            self.qtimer_camera()
        #PLC # ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ###########
        if self.plc!=-1:
            self.plc.go_Y_Z(self.y_value_go,self.z_value_go)
            self.uic.btnpulse_Y_go_in.pressed.connect(self.plc.set_Y_go_in_ON)
            self.uic.btnpulse_Y_go_in.released.connect(self.plc.set_Y_go_in_OFF)
            self.uic.btnPulse_Y_go_out.pressed.connect(self.plc.set_Y_go_out_ON)
            self.uic.btnPulse_Y_go_out.released.connect(self.plc.set_Y_go_out_OFF)
            self.uic.btnPulse_Z_go_up.pressed.connect(self.plc.set_Z_go_up_ON)
            self.uic.btnPulse_Z_go_up.released.connect(self.plc.set_Z_go_up_OFF)
            self.uic.btnPulse_Z_go_Down.pressed.connect(self.plc.set_Z_go_down_ON)
            self.uic.btnPulse_Z_go_Down.released.connect(self.plc.set_Z_go_down_OFF)
    def leExposuretime_edit(self):
        value=self.uic.leExposureTime.text()
        if value=='':
            self.uic.leExposureTime.setText('0')
        if value.isnumeric():
                self.uic.sldExposuretime.setValue(float(value))
    def leGama_edit(self):
        value=self.uic.leGama.text()
        if value=='':
            self.uic.leGama.setText('0')
        if value.isnumeric():
                self.uic.sldGamma.setValue(float(value))
    def leCH1_edit(self):
        value=self.uic.leCH1.text()
        if value=='':
            self.uic.leCH1.setText('0')
        if value.isnumeric():
                self.uic.sldCH1.setValue(float(value))
    def leCH2_edit(self):
        value=self.uic.leCH2.text()
        if value=='':
            self.uic.leCH2.setText('0')
        if value.isnumeric():
                self.uic.sldCH2.setValue(float(value))
                
    def login_plc(self):
        if self.plc!=-1:
            self.plc.setup_bit_ON()
    def logOut_plc(self):
        if self.plc!=-1:
            self.plc.setup_bit_OFF()
    def sldExposuretime_valueChanged(self):
        if self.uic.sldExposuretime.value()!='':
            self.uic.leExposureTime.setText(str(self.uic.sldExposuretime.value()))
    def sldGamma_valueChanged(self):
        if self.uic.sldGamma.value()!='':
            self.uic.leGama.setText(str(float(self.uic.sldGamma.value())))
    def load_model_setting(self):
            self.uic.lbmodelname.setText(self.modelname)
            dict=self.model_crtl.get_model_infor(self.modelname)
            self.uic.leDeepOfModel.setText(str(dict['deep']))
            self.uic.leCH1.setText(str(dict['Light']['CH1']))
            self.uic.leCH2.setText(str(dict['Light']['CH2']))
            self.uic.leExposureTime.setText(str(dict['camera']['ExposureTime']))
            self.uic.leGama.setText(str(dict['camera']['Gamma']))
            self.z_value_go=dict['Pulse']['Z_value']
            self.y_value_go=dict['Pulse']['Y_value'] 
#  set camera views
    def qtimer_camera(self):
        self.camera.SetParameter(KeyName.ExposureTime,'IFloat',float(self.uic.leExposureTime.text()))
        self.camera.SetParameter(KeyName.Gamma,'IFloat',float(self.uic.leGama.text()))
        self.camera.SetParameter(KeyName.GevSCPD,'IEnumeration',2000)
        self.camera.SetParameter(KeyName.PixelFormat,'IEnumeration',0x01080001)
        self.camera.StartGrabbing()
        timer=QTimer(self.main_win)
        timer.timeout.connect(self.view_camera)
        timer.start()
    def view_camera(self):
            ret = self.camera.GetFrameData()
            if ret == 0:
                img = self.camera.GetImageArray()
                self.setImage(img)
    def clear_code(self):
            for i in reversed(range(self.uic.v_layout.count())): 
                        self.uic.v_layout.itemAt(i).widget().deleteLater()
            
    def add_label_code(self,code):
        lbCode_temp = QtWidgets.QLabel()
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        lbCode_temp.setFont(font)
        lbCode_temp.setText(str(code))
        self.uic.v_layout.addWidget(lbCode_temp)
    def setImage(self,img):
            disply_width = self.uic.lbCameraview.width()
            display_height =  self.uic.lbCameraview.height()
            rgb_image=cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
            if self.captureimage:
                parent_dir = parameter.pathToFile
                pathtoImage=os.path.join(parent_dir,self.modelname+"/"+self.modelname+".jpg")
                cv2.imwrite(pathtoImage,rgb_image)
                # readcode 
                listcode=self.model_crtl.read_Code(rgb_image)
                self.listcode=listcode
                if len(listcode)>0:
                    for i in self.listcode:
                        self.add_label_code(str(i))
                else:
                    self.add_label_code("Not decoder!")

                self.captureimage = False
                self.path_to_imgage=pathtoImage
            imgresize=cv2.resize(rgb_image,(disply_width,display_height))
            h, w, ch = imgresize.shape
            bytes_per_line = ch * w
            convert_to_Qt_format = QtGui.QImage(imgresize.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
            p = convert_to_Qt_format.scaled(disply_width, display_height, Qt.KeepAspectRatio)
            pixmap= QPixmap.fromImage(p)
            self.uic.lbCameraview.setPixmap(pixmap)
# Set linght
    def setlight1(self):
        if self.light!=-1:
            self.light.setOne(1,int(self.uic.leCH1.text()))
    def setlight2(self):
        if self.light!=-1:
            self.light.setOne(2,int(self.uic.leCH2.text()))
    def setExposureTime(self):
        if self.camera!=-1:
            exposureTimeValue=float(self.uic.leExposureTime.text())
            self.camera.SetParameter(KeyName.ExposureTime,'IFloat',exposureTimeValue)
    def setGamma(self):
        if self.camera!=-1:
            gammaValue=float(self.uic.leGama.text())
            self.camera.SetParameter(KeyName.Gamma.replace(' ',''),'IFloat',gammaValue)
# save file
    def save_File(self):
        if self.plc!=-1:
            nameModel=self.modelname
            template= self.path_to_imgage
            deep=self.uic.leDeepOfModel.text()
            ch1=self.uic.leCH1.text()
            ch2=self.uic.leCH2.text() 
            exposureTime=self.uic.leExposureTime.text()
            gamma=self.uic.leGama.text()
            Y_value,Z_value,Distange_triger=self.plc.get_PLC_pulse()
            rs=self.model_crtl.fileclass.save_File(nameModel,template,deep,ch1,ch2,exposureTime,gamma,Z_value,Y_value,Distange_triger)
            if rs==1:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("save succesfully!")
                msg.setWindowTitle("infor")
                msg.exec_()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Critical)
            msg.setText("Not connect to PLC!")
            msg.setWindowTitle("Error")
            msg.exec_()
    def closeEvent(self, event):
        close = QMessageBox()
        close.setWindowTitle("Information")
        close.setText( "Are you sure want to exit?")
        close.setStandardButtons(QMessageBox.No|QMessageBox.Yes )
        close = close.exec()
        if close == QMessageBox.Yes:
            if self.camera!=-1:
                self.camera.StopGrabbing()
                self.camera.StopCamera()
            if self.light!=-1:
                self.light.Turn_Of_Light12()
                self.light.Close()
            if self.plc!=-1:
                self.plc.setup_bit_OFF()
            event.accept()
        else:
                event.ignore()
    def show(self):
        self.main_win.exec()
   
    def btnReadClicked(self):
        self.captureimage=True
        self.clear_code()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_win = Main_model_setting('b')
    main_win.show()
    sys.exit(app.exec())
