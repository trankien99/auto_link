import sys
import os
from PyQt5 import QtGui
from PyQt5 import QtWidgets

sys.path.insert(0,os.path.realpath(r'./Controller'))
sys.path.insert(0,os.path.realpath(r'./Views'))
sys.path.insert(0,os.path.realpath(r'./Lib'))
sys.path.insert(0,os.path.realpath(r'./Files'))
 
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_modelInfor import Ui_ModelManager
from Main_setting import Main_model_setting
class Main_model_infor():
    def __init__(self):
        self.main_win = QDialog()
        self.uic = Ui_ModelManager()
        self.uic.setupUi(self.main_win)
        self.model_crtl=ControlerClass()
        self.main_win.closeEvent=self.closeEvent
        self.event()
    def event(self):
        self.load_model_to_cbbox()
        self.uic.cbListModel.activated.connect(self.load_model_infor)
        self.uic.btnSetUpControl.clicked.connect(self.show_mosel_setting)
    def load_model_infor(self):
        nameModel=self.uic.cbListModel.currentText()
        dict=self.model_crtl.get_model_infor(nameModel)
        self.uic.lbmodeDeep.setText(str(dict['deep']))
        self.uic.lbModifile.setText(str(dict['modifiled']))
        self.uic.lbcreate.setText(str(dict['created']))
        self.uic.lbCH1.setText(str(dict['Light']['CH1']))
        self.uic.lbCH2.setText(str(dict['Light']['CH2']))
        self.uic.lbExposurettime.setText(str(dict['camera']['ExposureTime']))
        self.uic.lbgamma.setText(str(dict['camera']['Gamma']))
    
    def load_model_to_cbbox(self):
        listModel=self.model_crtl.get_models_from_server()
        self.uic.cbListModel.addItems(listModel)
    def show(self):
        self.main_win.exec()
    def show_mosel_setting(self): 
        modelname=self.uic.cbListModel.currentText()
        main_win = Main_model_setting(modelname)
        main_win.show()
 
    def closeEvent(self, event): 
            close = QMessageBox()
            close.setWindowTitle("Information")
            close.setText( "Are you sure want to exit?")
            close.setStandardButtons(QMessageBox.No|QMessageBox.Yes )
            close = close.exec()
            if close == QMessageBox.Yes:
                    event.accept()
            else:
                    event.ignore()
    # def get
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_win = Main_model_infor()
    main_win.show()
    sys.exit(app.exec())
