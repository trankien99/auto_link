
from ctypes import alignment
from datetime import datetime
import sys
import os
import threading
import time
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer, Qt

sys.path.append(os.path.realpath('Controller'))
sys.path.append(os.path.realpath('Views'))
sys.path.append(os.path.realpath('Lib'))
sys.path.append(os.path.realpath('Files'))
from HikCamera import KeyName
import parameter

from UI_home import Ui_LinkCode
import cv2
from linkCodeController import ControlerClass
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication,QMainWindow, QMessageBox
from Main_login import Main_login

class MainWindow():
        def __init__(self):
                self.main_win = QMainWindow()
                self.uic = Ui_LinkCode()
                self.uic.setupUi(self.main_win)
                self.main_win.closeEvent=self.closeEvent
                self.controler=ControlerClass()
                self.plc=self.controler.connect_to_PLC()
                self.camera=self.controler.connect_to_camera()
                self.linght=self.controler.connect_to_light()
                self.listcode=[]
                self.deep_model=0
                #camera parameter
                self.ExposureTime=0
                self.Gamma=0
                # Light parameter
                self.ch1=0
                self.ch2=0
                #PLC parameter
                self.Y_pulse=0
                self.Z_pulse=0
                self.Distange_triger=0
                # RUN     
                self._Run=False
                self.status_Start=False
                self.path_template=''
                # count of code
                self.countOfCode=0
                #
                self.countPass=0
                self.countFail=0
                self.total=0
                
                self.event()
        def event(self):
                self.Load_list_Model_for_Cbbox()
                self.uic.cbListmodel.activated.connect(self.load_model_infor)
                self.uic.btnStart.clicked.connect(self.start)
                self.uic.btnModel.clicked.connect(self.btn_Model_Clicked)
        def show(self):
                self.main_win.show()
        def closeEvent(self, event):
                close = QMessageBox()
                close.setText("Do you want to exit?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                close = close.exec()
                if close == QMessageBox.Yes:
                        if self.camera!=-1:
                            self.camera.StopCamera()
                        if self.linght!=-1:
                                self.linght.Turn_Of_Light12()
                                self.linght.Close()
                        if self.plc!=-1:
                                self.plc.setup_bit_OFF()
                        event.accept()
                else:
                        event.ignore()
        def clear_code(self):
                for i in reversed(range(self.uic.vLayout.count())): 
                        self.uic.vLayout.itemAt(i).widget().deleteLater()
        def add_label_code(self,code):
                font = QtGui.QFont()
                font.setPointSize(13)
                font.setBold(True)
                font.setWeight(75)
                lbCode = QtWidgets.QLabel()
                lbCode.setObjectName("lbCode")
                lbCode.setText(code)
                lbCode.setFont(font)
                self.uic.vLayout.addWidget(lbCode) 
         
        def btn_Model_Clicked(self):
                main_win = Main_login()
                main_win.show()
        def Load_list_Model_for_Cbbox(self): 
                listmodel= self.controler.fileclass.load_List_Models()
                self.uic.cbListmodel.addItems(listmodel)
        def load_model_infor(self):
                modelname=self.uic.cbListmodel.currentText()
                # path='/home/pi/json/models/'
                # path_to_file=glob.glob(path+modelname+'/'+modelname+'*.json')
                # print(path_to_file[0])
                # with open(path_to_file[0], 'r') as f:
                #         dict=json.loads(f.read())
                dict=self.controler.get_model_infor(modelname)
                self.countOfCode=dict['Count']
                self.deep_model =dict['deep']
                self.ch1= dict['Light']['CH1']
                self.ch2=dict['Light']['CH2']
                self.ExposureTime=dict['camera']['ExposureTime']
                self.Gamma=dict['camera']['Gamma']
                self.Z_pulse=dict['Pulse']['Z_value']
                self.Y_pulse= dict['Pulse']['Y_value']
                self.path_template=dict['Template']
                self.Distange_triger=dict['Ditange_trigger']
                
        def start(self):
                res_plc = self.check_PLC()
                res_cam=self.check_camera()
                res_light=self.check_light()
                if res_plc==1 and res_cam==1 and res_light==1:
                        if self.status_Start==False:
                                self._Run=True
                                icon4 = QtGui.QIcon()
                                icon4.addPixmap(QtGui.QPixmap("./Icons/stop.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                                self.uic.btnStart.setIcon(icon4)
                                self.status_Start= True
                        elif self.status_Start:
                                self._Run=False 
                                icon4 = QtGui.QIcon()
                                icon4.addPixmap(QtGui.QPixmap("./Icons/start.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                                self.uic.btnStart.setIcon(icon4)
                                self.status_Start= False
                        self.plc.sendCoordiante(self.Y_pulse,self.Z_pulse,self.Distange_triger)
                        # self.thresh_Main()
                        self.timer_start()
        def timer_start(self):
                try:
                        self.template=cv2.imread(self.path_template,1)
                        timer=QTimer(self.main_win)
                        timer.timeout.connect(self.main)
                        timer.start(100)
                except Exception as ex:
                        print(ex)
                        if self.camera!=-1:
                                self.camera.StopCamera()
                        if self.linght!=-1:
                                self.linght.Turn_Of_Light12()
                                self.linght.Close()
                        if self.plc!=-1:
                                self.plc.setup_bit_OFF()
                        self.main_win.close()
                        
        def main(self):
                if self.plc.get_trigger_startgrap()==1:
                        self.camera.StartGrabbing()
                check_triger=self.plc.get_trigger_capture_image()
                print('Trigger',check_triger)
                if check_triger ==1:
                        img=self.capture_Image()
                        self.plc.set_go_up_after_capture()
                        self.plc.reset_trigger_capture_image()
                        self.clear_code()
                        self.camera.StopGrabbing()

                        # self.uic.lbCamview.clear()
                        # self.clear_image()
                        self.plc.reset_trigger_startgrap()
                        self.set_Image(img)
                        modelname=self.uic.cbListmodel.currentText()
                        listcode,imagealign=self.controler.read_Code_main(img,self.template)
                        if imagealign.shape[0] ==0:
                                self.plc.setFail()
                                self.uic.lbStatus.setText('Fail')
                                self.uic.lbStatus.setStyleSheet("background:rgb(255, 0, 0)")
                                self.add_label_code('not decoder!')
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText("Cant align image!")
                                msg.setWindowTitle("Error")
                                msg.exec_()
                                self.countFail+=1
                        else:
                                if self.countOfCode==len(listcode):
                                        self.save_image_align(imagealign,modelname,'Pass')
                                        self.listcode=listcode
                                        self.plc.setPass()
                                        self.uic.lbStatus.setText('Pass')
                                        self.uic.lbStatus.setStyleSheet("background:rgb(0, 255, 0)")
                                        self.countPass+=1
                                else:
                                        self.save_image_align(imagealign,modelname,'Fail')
                                        self.plc.setFail()
                                        self.uic.lbStatus.setText('Fail')
                                        self.uic.lbStatus.setStyleSheet("background:rgb(255, 0, 0)")
                                        self.countFail+=1
                                for i in listcode:
                                                self.add_label_code(str(i))
                        self.total+=1
                        self.show_cout()
        def clear_image(self):
                self.clear_code()
                # self.uic.lbCamview.clear()
                self.uic.lbStatus.setText('Processing')
                self.uic.lbStatus.setStyleSheet("background:rgb(255, 255, 0)")
        
        def save_image_align(self,img,modelname,status):
                timenow=datetime.now()
                parent_dir = "/home/"
                path1 = os.path.join(parent_dir,"pi")
                if not os.path.exists(path1):
                        os.mkdir(path1)
                path2 = os.path.join(path1,'ImageAlign')
                if not os.path.exists(path2):
                        os.mkdir(path2)
                path3 = os.path.join(path2,modelname)
                if not os.path.exists(path3):
                        os.mkdir(path3)
                path4 = os.path.join(path3,status)
                if not os.path.exists(path4):
                        os.mkdir(path4)
                cv2.imwrite(path4+'/'+str(timenow)+'.jpg',img)
        def show_cout(self):
                self.uic.lbPass.setText(str(self.countPass))
                self.uic.lbFail.setText(str(self.countFail))
                self.uic.lbYeildrate.setText(str(round((self.countPass/self.total*100),2)))
        def capture_Image(self):
                time.sleep(0.4)
                img=None
                ret = self.camera.GetFrameData()
                if ret == 0:
                        img = self.camera.GetImageArray()
                        
                        img=cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
                return img

        def set_Image(self,img):
                disply_width = self.uic.lbCamview.width()
                display_height =  self.uic.lbCamview.height()
                # rgb_image=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
                imgresize=cv2.resize(img,(disply_width,display_height))
                h, w, ch = imgresize.shape
                bytes_per_line = ch * w
                convert_to_Qt_format = QtGui.QImage(imgresize.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
                p = convert_to_Qt_format.scaled(disply_width, display_height, Qt.KeepAspectRatio)
                pixmap= QtGui.QPixmap.fromImage(p)
                self.uic.lbCamview.setPixmap(pixmap)
        def check_PLC(self):
                if self.plc!=-1: 
                        self.uic.lbPCL.setText("Connected")
                        self.uic.lbPCL.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                        return 1
                else :
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Not connect to PLC!")
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1

 ################# Camera ###########################
        def check_camera(self):
                if self.camera !=-1:  
                        self.uic.lbCamera.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                        self.uic.lbCamera.setText("Connected")
                        self.camera.SetParameter(KeyName.ExposureTime,'IFloat',float(self.ExposureTime))
                        self.camera.SetParameter(KeyName.Gamma,'IFloat',float(self.Gamma))
                        self.camera.SetParameter(KeyName.GevSCPD,'IEnumeration',2000)
                        self.camera.SetParameter(KeyName.PixelFormat,'IEnumeration',0x01080001)
                        return 1
                else:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Not connect to camera!")
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1
 ################# Light ###########################
        def check_light(self):
                if self.linght!=-1:
                        self.linght.Turn_On_Light12()
                        self.linght.setOne(1,int(self.ch1))
                        self.linght.setOne(2,int(self.ch2))
                        self.uic.lbLinght.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                        self.uic.lbLinght.setText("Connected")
                        return 1
                else:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Not connect to Light!")
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_win = MainWindow()
    main_win.show()
    sys.exit(app.exec())