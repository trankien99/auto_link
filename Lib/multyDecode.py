import os
import PIL 
import cv2
import numpy as np 
from pyzbar.pyzbar import decode, bounding_box
import imutils
from PIL import Image
from datetime import datetime
from parameter import parameter
class Multy_Decode():
        
    def Decode(self,img, k_blur = 1):
        # print("scan code " + str(img.shape))
        # img = cv2.cvtColor(img,cv2.COLOR_BGR)
        sn = "NOT FOUND"
        rect = (0,0,0,0)
        img = cv2.medianBlur(img, k_blur)
        # cv2.imwrite("test1.png", img)
        try:
            rows,cols = img.shape
            result_data = []
            for n in range(1, 5 ,1):
                if(len(result_data) > 0):
                    break
                if( n > 1):
                    resized_img= cv2.resize(img,(n*rows,n*cols))
                else:
                    resized_img = img
                for al_val in range(0,25, 2):
                    alpha = 1 + al_val*0.1
                    result_img = cv2.convertScaleAbs(resized_img, alpha=alpha, beta=0)
                    result_data = decode(result_img)
                    print('quality code in image: ',len(result_data))
                    if(len(result_data) > 0):
                        list_sn = []
                        for code in result_data:
                            sn = code.data.decode("utf-8")
                            list_sn.append(sn)
                        break
                
            return list_sn
        except Exception as err:
            print(err)
            return sn, rect
    def simpleDecode(self,image):
        list_sn = []
        codes = decode(image)
        print('quatity code in image: ',len(codes))
        for code in codes:
            sn = code.data.decode("utf-8")
            list_sn.append(sn)
        return list_sn
    def templateMatching(self,image,template):
        w, h = template.shape[::-1]
        res = cv2.matchTemplate(img,template,cv2.TM_SQDIFF_NORMED)
        print(res)
        # threshold = 0.8
        # loc = np.where( res >= threshold)
        # print(loc)
        # for pt in zip(*loc[::-1]):
        #     top_left = pt
        #     bottom_right = (pt[0] + w, pt[1] + h)
            # cv.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        print(max_val)
        top_left = min_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        mac = (top_left,bottom_right)
        print(mac)
        return mac  
    def align_images(self,image, template, maxFeatures=500, keepPercent=0.5,
        debug=False):
        # convert both the input image and template to grayscale
        imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        templateGray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
        cv2.imwrite("/home/pi/test.jpg",imageGray)
        # use ORB to detect keypoints and extract (binary) local
        # invariant features
        orb = cv2.ORB_create(maxFeatures)
        (kpsA, descsA) = orb.detectAndCompute(imageGray, None)
        (kpsB, descsB) = orb.detectAndCompute(templateGray, None)
        # match the features
        method = cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING
        matcher = cv2.DescriptorMatcher_create(method)
        matches = matcher.match(descsA, descsB, None)
        # sort the matches by their distance (the smaller the distance,
        # the "more similar" the features are)
        matches = sorted(matches, key=lambda x:x.distance)
        # keep only the top matches
        keep = int(len(matches) * keepPercent)
        matches = matches[:keep]
        # check to see if we should visualize the matched keypoints
        
        if debug:
            matchedVis = cv2.drawMatches(image, kpsA, template, kpsB,
                matches, None)
            matchedVis = imutils.resize(matchedVis, width=1000)
            cv2.imshow("Matched Keypoints", matchedVis)
            cv2.waitKey(0)
        # allocate memory for the keypoints (x, y)-coordinates from the
        # top matches -- we'll use these coordinates to compute our
        # homography matrix
        aligned = np.array([])
        if(len(matches)>0):
            ptsA = np.zeros((len(matches), 2), dtype="float")
            ptsB = np.zeros((len(matches), 2), dtype="float")
            # loop over the top matches
            for (i, m) in enumerate(matches):
                # indicate that the two keypoints in the respective images
                # map to each other
                ptsA[i] = kpsA[m.queryIdx].pt
                ptsB[i] = kpsB[m.trainIdx].pt
            # compute the homography matrix between the two sets of matched
            # points
            (H, mask) = cv2.findHomography(ptsA, ptsB, method=cv2.RANSAC)
            # use the homography matrix to align the images
            (h, w) = template.shape[:2]
            aligned = cv2.warpPerspective(image, H, (w, h)) 
        # return the aligned image
        return aligned                                                                                                                  

if __name__ == "__main__":
    dc=Multy_Decode()
    img = cv2.imread("/home/pi/json/models/a/a.jpg")
    # img_pil = cv2.imread('/home/pi/ImageAlign/a/Fail2021-11-29 23:38:52.650300.jpg')
    img_test = cv2.imread("/home/pi/test.jpg")
#     # _,img_thresh = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
    # w,h = img_pil.shape[:2]
    img_aligned = dc.align_images(img_test,img,debug=True)
    img_thresh = cv2.resize(img_aligned,(720,1080
    ))
    sn = dc.simpleDecode(img_thresh)
    print(sn)
#     img_show = cv2.resize(img_test,(1080,720))
    cv2.imshow('img2',img_aligned)
    
    cv2.waitKey(0)
    cv2.destroyAllWindows()