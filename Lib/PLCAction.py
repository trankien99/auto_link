from  SLMP import *
import os
import time


class Bit():
    def __init__(self):
        self.setup="S10"
        self.Y_up="M300" 
        self.Y_back="M301"

        self.Z_Down="M302" 
        self.Z_Up="M303"

        self.trigger_capture_image="M1008"
        self.trigger_has_product="M1010"
        
        self.Link_pass="M1002"
        self.Link_fail="M1003"
        self.bit_go_up_after_capture='M1009'
class Register():##Register 32 bit
    def __init__(self):
        self.Y_W="D400"
        # self.Y_Read="SD5502"
        # self.Y_Read="D1010"
        self.Y_Read="SD5502"
        self.Z_W="D402"
        # self.Z_Read="SD5542"
        # self.Z_Read="D1020"
        self.Z_Read="SD5542"

        self.start_point="M335"
        self.finish_point="M336"
        self.Distange_trigger_R="D224"
        # MAIN
        self.Distange_trigger_W="D220"
        self.Z_WRITE_MAIN = 'D600'
        self.Y_WRITE_MAIN = 'D500'


class PLC_Action_Class():
    def __init__(self,ip,port):
        self.bit=Bit()
        self.regis=Register()
        self.plc=PLCController(ip,port)
    def check_plc(self):
        rs=self.plc.GetDevice('M7000')
        return rs
    def getSetup_bit(self):
        a=self.plc.GetDevice(self.bit.setup)
        return a
    def setup_bit_ON(self):
        if self.getSetup_bit()==0:
            self.plc.SetDevice(self.bit.setup,1)
        else :
            return -1
    def setup_bit_OFF(self):
        if self.getSetup_bit()==1:
            self.plc.SetDevice(self.bit.setup,0)
        else:
            return -1
    # Z control

    def get_Z_go_up(self):
        a=self.plc.GetDevice(self.bit.Z_Up)
        return a
    def set_Z_go_up_ON(self): 
        if self.get_Z_go_up()==0:
            self.plc.SetDevice(self.bit.Z_Up,1)
        else:
            return -1
    def set_Z_go_up_OFF(self):
        if self.get_Z_go_up()==1:
            self.plc.SetDevice(self.bit.Z_Up,0)
        else:
            return -1

    def get_Z_go_down(self): 
        return self.plc.GetDevice(self.bit.Z_Down)

    def set_Z_go_down_ON(self):
        if self.get_Z_go_down()==0:
            self.plc.SetDevice(self.bit.Z_Down,1)
        else:
            return -1
    def set_Z_go_down_OFF(self):
        if self.get_Z_go_down()==1:
            self.plc.SetDevice(self.bit.Z_Down,0)
        else:
            return -1
# Y control
    def get_Y_go_out(self): 
        return self.plc.GetDevice(self.bit.Y_up)
    def set_Y_go_out_ON(self):
        if self.get_Y_go_out()==0:
            self.plc.SetDevice(self.bit.Y_up,1)
        else:
            return -1
    def set_Y_go_out_OFF(self):
        if self.get_Y_go_out()==1:
            self.plc.SetDevice(self.bit.Y_up,0)
        else:
            return -1
    def get_Y_go_in(self):
        return self.plc.GetDevice(self.bit.Y_back)
    def set_Y_go_in_ON(self):
        if self.get_Y_go_in()==0:
            self.plc.SetDevice(self.bit.Y_back,1)
        else:
            return -1
    def set_Y_go_in_OFF(self):
        if self.get_Y_go_in()==1:
            self.plc.SetDevice(self.bit.Y_back,0)
        else:
            return -1
   
    # start and finish point

    def get_start_Point(self):
        return self.plc.GetDevice(self.bit.start_point)
    def set_start_Point_ON(self):
        if self.get_start_Point()==0:
            self.plc.SetDevice(self.bit.start_point,1)
        else:
            return -1
    def set_start_Point_OFF(self):
        if self.get_start_Point()==1:
            self.plc.SetDevice(self.bit.start_point,0)
        else :
            return -1

    def get_finish_Point(self):
        return self.plc.GetDevice(self.bit.finish_point)
    def set_finish_Point_ON(self):
        if self.get_finish_Point()==0:
            self.plc.SetDevice(self.bit.finish_point,1)
        else:
            return -1
    def set_finish_Point_OFF(self):
        if self.get_finish_Point()==1:
            self.plc.SetDevice(self.bit.finish_point,0)
        else :
            return -1
    def set_go_up_after_capture(self):
        self.plc.SetDevice(self.bit.bit_go_up_after_capture,1)

    def get_trigger_capture_image(self):
        return self.plc.GetDevice(self.bit.trigger_has_product)
    
    #
    def setFail(self):
        self.plc.SetDevice(self.bit.Link_fail,1)
    def setPass(self):
        self.plc.SetDevice(self.bit.Link_pass,1)
    def get_trigger_startgrap(self):
        res=self.plc.GetDevice(self.bit.trigger_has_product)
        return res
    def reset_trigger_startgrap(self):
            self.plc.SetDevice(self.bit.trigger_has_product,0)
    def get_trigger_capture_image(self):
            return self.plc.GetDevice(self.bit.trigger_capture_image)
    def reset_trigger_capture_image(self):
        return self.plc.SetDevice(self.bit.trigger_capture_image,0)

    ####################____SET UP  Register_____#################################

    def get_Z_read(self):
        rs=self.plc.GetDevice2(self.regis.Z_Read)
        return rs
    def get_Y_read(self):
        rs=self.plc.GetDevice2(self.regis.Y_Read)
        return rs
    def go_Y_Z(self,Y_value,Z_value,timeout=5):
        rs=self.plc.SetDevice2(self.regis.Y_W,Y_value)
        if rs==1:
            rs1=self.plc.SetDevice2(self.regis.Z_W,Z_value)
            if rs1==1:
                self.plc.SetDevice(self.regis.start_point,1)
        starttime=time.time()
        del_t=0
        res_return = -1
        while del_t<=timeout:
            rs=self.plc.GetDevice(self.regis.finish_point)
            afterTime=time.time()
            del_t=afterTime-starttime
            if del_t>timeout:
                break
            if rs ==1:
                res_return=1
                r=self.plc.SetDevice(self.regis.finish_point,0)
                break
        return res_return

    def setDistange_W(self,value):
        rs=self.plc.SetDevice2(self.regis.Distange_trigger_W,value)
    def getDistange_R(self):
        rs=self.plc.GetDevice2(self.regis.Distange_trigger_R)
        return rs
    
    def sendCoordiante(self,coor_y,coor_z,value_pulse):
        self.plc.SetDevice2(self.regis.Distange_trigger_W,value_pulse)
        self.plc.SetDevice2(self.regis.Z_WRITE_MAIN,coor_z)
        self.plc.SetDevice2(self.regis.Y_WRITE_MAIN,coor_y)

    def get_PLC_pulse(self):
        yread=self.get_Y_read()
        zread=self.get_Z_read()
        distangle=self.getDistange_R()
        return yread,zread,distangle
    
# if __name__ =='__main__':
#     plc=PLC_Action_Class('192.168.1.4',5004)
#     a=plc.check_plc()
#     c=plc.get_Z_read()
#     b=plc.get_Y_read()
#     print(c)
#     print(b)

#     print(a)
#     plc.setup_bit_ON()
#     plc.set_Z_go_down_ON()
#     time.sleep(2)
#     plc.set_Z_go_down_OFF()
#     plc.setup_bit_OFF()
    # plc.setup_bit_ON()
    # # a=plc.get_Z_read()
    # # b=plc.get_Y_read()
    # a=plc.go_Y_Z(33877,20000,5)
    # print(a)
    # plc.setup_bit_OFF()