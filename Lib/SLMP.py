import time as time_obj
import socket
from threading import Thread
import threading
import os
import sys

class PLCController:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.subheader = "5000"
        self.network = "00"
        self.station = "FF"
        self.moduleio = "03FF"
        self.multidrop = "00"
        self.reserved = "0010"
        self.write = "1401"
        self.read = "0401"
        self.bit = "0001"
        self.word = "0000"
        self.listsp = "MYZDLFVBSW"
        self.tlock = threading.Lock()

    def SendCommand(self, cmd):
        self.tlock.acquire()
        result = "-1"
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(0.2)
        try:
            time_obj.sleep(0.01)
            client.connect((self.ip, self.port))
            client.send(cmd.encode('utf-8'))
            result = client.recv(1024).decode('utf-8')
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            print("test:" + str(e))
            result = "-1"
        finally:
            self.tlock.release()
        return result

    def SetDevice(self, device, value): # set value cho bit tren plc (0,1)
        acommand = ""
        bcommand = ""
        ccommand = ""
        dev_type_1 = device[0:1].upper()
        dev_type_2 = device[0:2].upper()
        if ((value > 65535) & (value < 0)):
             return -1
        if(dev_type_1 in self.listsp):
            acommand = self.subheader + self.network + \
                self.station + self.moduleio + self.multidrop
            ccommand = "D000" + self.network + self.station + self.moduleio + self.multidrop
            length = 24
            if(dev_type_1 == "D") | (dev_type_1 == "W"):
                length = length + 4
                bcommand = acommand + "{:04X}".format(length) + self.reserved + self.write + self.word + dev_type_1 + "*"\
                     + "{:06d}".format(int(device[1:])) + \
                                       "0001" + "{:04X}".format(value)
            elif(dev_type_2 == "SD"):
                length = length + 4
                bcommand = acommand + "{:04X}".format(length) + self.reserved + self.write\
                    + self.word + dev_type_2 + "{:06d}".format(int(device[2:]))\
                    + "0001" + "{:04X}".format(value)
                return -1
            else:
                if(value == 1) | (value == 0):
                    length = length + 1
                    bcommand = acommand + "{:04X}".format(length) + self.reserved + self.write \
                        + self.bit + dev_type_1 + "*" + "{:06d}".format(int(device[1:])) \
                            + "0001" + "{:01X}".format(value)
                else:
                    return -1
            try:
                result = self.SendCommand(bcommand)
                if(result == "-1"):
                    result = -1
                if(int(result[len(ccommand)+4:]) == 0):
                    return 1
                else:
                    return -1
            except:
                return -1
        else:
            return -1
    def SetDevice2(self,device,value):
        str_reg_hight = ''
        res_1=0
        res_2=0
        if 'SD' in device:
            str_reg_hight = 'SD'+str(int(device[2:])+1)
           
        elif 'D' in device:
            str_reg_hight = 'D'+str(int(device[1:])+1)
        binary32bit=self.decimalToBinary(value)
        if len(binary32bit)>16:
            binary1=binary32bit[len(binary32bit)-16:] 
            binary2=binary32bit[0:len(binary32bit)-16]
            dec1=self.binaryToDecimal(binary1)
            dec2=self.binaryToDecimal(binary2)
          
            for i in range(3):
                res_1 = self.SetDevice(device,dec1)
                res_2 = self.SetDevice(str_reg_hight,dec2)
                value_got = self.GetDevice2(device)
                if(value_got==value):
                    break
        else:
            for i in range(3):
                res_1 = self.SetDevice(device,value)
                res_2 = self.SetDevice(str_reg_hight,0)
                value_got = self.GetDevice2(device)
                if value_got == value:
                    break
        if res_1==-1 or res_2==-1:
            return -1  
        else:
            return 1

 
    def GetDevice(self, device):   # get value cua bit tren plc (0,1)
        value = -1
        acomand = ""
        bcommand = ""
        ccommand = ""
        dev_type_1 = device[0:1].upper()
        dev_type_2 = device[0:2].upper()
        if(dev_type_1 in self.listsp):
            acomand = self.subheader + self.network + \
                self.station + self.moduleio + self.multidrop
            ccommand = "D000" + self.network + self.station + self.moduleio + self.multidrop
            length = 24
            if((dev_type_1 == "D") | (dev_type_1 == "W")):
                bcommand = acomand + "{:04X}".format(length) \
                    + self.reserved + self.read + self.word + dev_type_1 + "*" \
                    + "{:06d}".format(int(device[1:])) + "0001"
            elif(dev_type_2 == "SD"):
                bcommand = acomand + "{:04X}".format(length) \
                    + self.reserved + self.read + self.word + dev_type_2 + "{:06d}".format(int(device[2:]))\
                    + "0001"
            else:
                bcommand = acomand + "{:04X}".format(length) \
                    + self.reserved + self.read + self.bit + dev_type_1 + "*"\
                    + "{:06d}".format(int(device[1:])) + "0001"
            try:
                result = self.SendCommand(bcommand)
                if(result == "-1"):
                    return -1
                if(int(result[len(ccommand)+4:len(ccommand)+8], base=16) == 0):
                    value = int(result[len(ccommand)+8:], base=16)
            except Exception as err:
                #print(err)
                return value
            return value

        else:
            return value

    def GetDevice2(self,device): #get value cua thanh ghi 32bit
        str_reg_hight = ''
        if 'SD' in device:
            str_reg_hight = 'SD'+str(int(device[2:])+1)
        elif 'D' in device:
            str_reg_hight = 'D'+str(int(device[1:])+1)
        value1 = self.GetDevice(device)
        value2 = self.GetDevice(str_reg_hight)
        str_binary1 = self.decimalToBinary(value1)
        if len(str_binary1)<16:
            for i in range(16-len(str_binary1)):
                str_binary1='0'+str_binary1
        # print(str_binary1)
        str_binary2 = self.decimalToBinary(value2)
        # print(str_binary2)
        binary32=str_binary2+str_binary1
        value=None
        if binary32!='':
            value = self.binaryToDecimal(binary32)
        return value
    def decimalToBinary(self,ip_val):
        a = []
        if(ip_val>=1):       
            while True:
                a.append(str(ip_val%2))
                ip_val = ip_val//2
                
                if(ip_val==0):
                    break
        a.reverse()
        return ''.join(a)
        # return bin(ip_val).replace("0b","")
    def binaryToDecimal(self,val): 
        return int(val,2) 

