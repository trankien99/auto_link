
from os import close
from LightCrtl import *
from parameter import parameter

class LightAction():
    def __init__(self):
        self.__myLight=DKZ224V4ACCom(parameter.light_portname)
    def Open(self):
        check=self.__myLight.open()
        if check==0:
            return 1
    def Close(self):
        check=self.__myLight.close()
        if check==0:
            return 1
    def Turn_On_Light12(self):
        self.__myLight.ActiveOne(1,1)
        self.__myLight.ActiveOne(2,1)
    def Turn_Of_Light12(self):
        self.__myLight.ActiveOne(1,0)
        self.__myLight.ActiveOne(2,0)
    def setOne(self,chanel,value):
        self.__myLight.SetOne(chanel,value)

# l=LightAction()
# l.Open()
# l.Turn_On_Light12()
# l.setOne(1,255)
# l.setOne(2,255)
# time.sleep(5)
# l.Turn_Of_Light12()
# l.Close()