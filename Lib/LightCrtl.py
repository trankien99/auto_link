  
import serial    
import time 

import serial.tools.list_ports

class DKZ224V4ACCom():
    def __init__(self,portname):
            self.PortName = portname
            self.BaudRate = 19200
            self.se = serial.Serial(self.PortName,self.BaudRate,timeout=.1)

    def open(self):
        if not self.se.isOpen():
            try:
                    self.se.write_timeout(200)
                    self.se.read(2)
                    time.sleep(5)
                    self.se.open()
                  
            except:
                   return -1
            return 0
        return 0

    def close(self):
        if self.se.isOpen():
            try:
                self.se.close()
            except:
                return -1
            return 0
        return 0
    def ActiveOne(self,chanel,Mode):
        data=[171,186,3,50,chanel-1,Mode] 
        succ = False
        try:
            self.se.write(data)
            response = self.se.read(9)
            if (response.decode() == 'ok' or response == b'0x6f'):
                succ = True
            self.se.flush()
            # self.se.flush()
        except:
                return -1
        if succ:
            return 0
        else: 
            return -2
    def SetOne( self,Channel, Value):  
        data= [0xab, 0xba, 0x03, 0x31, Channel-1, Value]
        # data=[171,186,3,49,Channel-1,Value] 
        succ = False
        try:
            self.se.write(data)
            response = self.se.readline()
            if (response.decode() == 'ok' ):
                succ = True
                self.se.flushInput()
                self.se.flushOutput() 
        except:
                return -1
        if succ:
            return 0
        else: return -2
    def SetFour(self,value1,value2,value3,value4):
        data=[0xab, 0xba, 0x05, 0x33, value1,value2,value3,value4]
        succ = False
        try:
            self.se.write(data)
            time.sleep(.1)
            response = self.se.readline()
            if (response.decode() == 'ok' ):
                succ = True
                self.se.flushInput()
                self.se.flushOutput() 
        except:
                return -1
        if succ:
            return 0
        else: return -2

    def Activefour(self,model1,mode2,mode3,mode4):
        data =[0xab, 0xba, 0x05, 0x34, model1, mode2, mode3, mode4 ]
        succ = False
 
        try:
            self.se.write(data)
            time.sleep(0.1)
            response = self.se.read(9)
            
            if (response.decode() == 'ok'):
                succ = True
            self.se.flushInput()
            self.se.flushOutput() 
        except:
                return -1
        if succ:
            return 0
        else: 
            return -2

    def getComPort(self): 
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))

# dk=DKZ224V4ACCom('/dev/ttyUSB0')


# c=dk.open()
# # print(c)
# c=dk.ActiveOne(1,1)
# # c=dk.ActiveOne(2,1)
# # print(c)
# dk.SetOne(2,0)
# time.sleep(10)

# dk.Activefour(1,1,1,1)
# dk.SetFour(255,255,255,255) 

# c=dk.close()
# print(c)