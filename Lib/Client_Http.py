
import requests
import json
import datetime

class Http_client_Class():
    def __init__(self,url="http://192.168.1.12:8888/"):
        self.url=url
    def get(self,param):
        check=False
        jsontext=''
        try:
            url = self.url+param
            payload={}
            headers = {
                'id':'link'
            }
            response = requests.request("GET", str(url), headers=headers, data=payload,verify=False)
            if response!='':
                jsontext=json.loads(response.text)
                check=True
            
        except:
                check=False

        return jsontext,check

# c=Http_client("http://192.168.1.12:8888/")
# c.get('users')
# c.get('models')
# # c.get('snbox')
