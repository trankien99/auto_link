import datetime
import sys
import os
sys.path.append(os.path.realpath('Lib'))
import glob
from parameter import *
import json

class FileClass():
    def __init__(self):
        self.path=parameter.pathToFile
    def load_File(self,modelname):
        dict={}
        pathfull=os.path.join(self.path,modelname+'/'+modelname+'.json')
        with open(pathfull, 'r') as f:
                dict=json.loads(f.read())
        return dict

    def load_List_Models(self):
        dir=glob.glob(self.path+'/*')
        listmodel= [i.split('/')[-1] for i in dir]
        # print(listmodel)
        return listmodel

    def save_File(self,modelName,count,Template,deep,CH1,CH2,ExposureTime,Gamma,Z_value,Y_value,Distange_triger):
        path1=os.path.join(self.path,modelName+"/"+modelName+".json")
        try:
            with open(path1, 'r+') as file:
                dict=json.loads(file.read())
                dict=self.load_File(modelName)
                dict['Count']=count
                dict['Template'] = Template
                dict['deep']=deep
                dict['modifiled']= str(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                dict['Light']['CH1']=CH1
                dict['Light']['CH2']=CH2
                dict['Pulse']['Z_value'] = Z_value
                dict['Pulse']['Y_value'] = Y_value
                dict['Ditange_trigger'] = Distange_triger
                dict['camera']['ExposureTime']=ExposureTime
                dict['camera']['Gamma']=Gamma
                file.seek(0)
                json.dump(dict, file, indent=4)
                file.truncate()
                return 1
        except:
            return -1
    def create_File_Json(self,path,name):
            dictcam={}
            dictcam['ExposureTime']=3000
            dictcam['Gamma']=1
            dictlight={}
            dictlight['CH1']=127
            dictlight['CH2']=127
            dictcPulse={}
            dictcPulse['Z_value'] = 0
            dictcPulse['Y_value'] = 30000
            dict = {}
            dict['name'] = name
            dict['Count']=0
            dict['Template'] = ''
            dict['deep'] = 0
            dict['Ditange_trigger'] = 0
            dict['modifiled'] = ''
            dict['created'] = str(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            dict['camera'] = dictcam
            dict['Light'] = dictlight
            dict['Pulse'] = dictcPulse
            with open(path, "w") as f:
                json.dump(dict,f)
