import datetime
import json
import sys
import os
import cv2 
sys.path.insert(0,os.path.realpath('Views'))
sys.path.insert(0,os.path.realpath('Lib'))
sys.path.insert(0,os.path.realpath('Files'))

from PLCAction import PLC_Action_Class
from multyDecode import Multy_Decode
from LightAction import LightAction
from Client_Http import *
from parameter import *
from Client_Http import *
from File import *
from HikCamera import *

from PyQt5.QtWidgets import QMainWindow

class ControlerClass():
    def __init__(self):
        # super.__init__()
        self.fileclass=FileClass()
        self.UrlServer=parameter.UrlServer
        self._camera=HikCamera()
        self.captureimage=False
        # pcl parameter
        self.path_to_imgage=''
        self.Z_value=0
        self.Y_value=0
        self.z_value_view=0
        self.y_value_view=0
        self.Distange_triger=0

        self.modelName=''
        self.pathtoimgage=''
        self.de=Multy_Decode()
        self.get_models_from_server()
        #int PLC ,camera

    def get_users(self): #Get user and password for login form
        try:
            client=Http_client_Class()
            jtext=client.get('users')
            username=''
            pas=''
            if jtext['status']=='OK':
                username=jtext['usersname']
                pas=jtext['password']
                return username,pas
        except:
            return -1
    def get_model_infor(self,nameModel):
        dict= self.fileclass.load_File(nameModel)
        return dict
    
    def get_models_from_server(self): # get list model from server and create file json  
            parent_dir = "/home/"
            path1 = os.path.join(parent_dir,"pi")
            if not os.path.exists(path1):
                os.mkdir(path1)
            path2 = os.path.join(path1,"json") 
            if not os.path.exists(path2):
                os.mkdir(path2)
            path3 = os.path.join(path2,"models")
            if not os.path.exists(path3):
                os.mkdir(path3)

            try:
                # client=Http_client_Class(self.UrlServer)
                # jtext=client.get('models')
                # if jtext['status']=='OK':
                #     listModel=jtext['values']
                listModel=["a","b","c"] #nhap

                if len(listModel)>0:
                        for i in listModel:          
                            # parent_dir = parameter.pathToFile
                            path = os.path.join(path3,i) 
                            if not os.path.exists(path):
                                os.mkdir(path)
                            path1 = os.path.join(path,i+'.json') 
                            if not os.path.exists(path1):
                                self.fileclass.create_File_Json(path1,i)
                else:
                    return -1
            except:
                return -1
            return listModel

################# PCL ###########################
    def connect_to_PLC(self):
            try:
                    plc=PLC_Action_Class(parameter.plc_ip,parameter.plc_port)
                    check=plc.check_plc()
                    if(check==1): 
                            return plc
                    else :
                            return -1
            except Exception as bug:
                    return -1
################# camera ###########################
    def connect_to_camera(self):
                try:
                        name,devic=list(self._camera.GetDevicesList())
                        if len(devic)>0:
                            self._camera.OpenCamera(devic[0])
                           
                            return self._camera
                            
                        else:
                                return -1
                except:
                        return -1
################# Light ###########################
    def connect_to_light(self):
            try:
                    light=LightAction()
                    rs=light.Open()
                    if rs==1:
                            return light
                    else:
                            
                            return -1
            except:
                    return -1
# readcode
    def read_Code(self,img):
        listCode=self.de.simpleDecode(img)
        return listCode
    def read_Code_main(self,img,template):
        listCode = []
        imgaligned=self.de.align_images(img,template)
        if imgaligned.shape[0]!=0:
            listCode=self.de.simpleDecode(imgaligned)
            # return listCode,imgaligned
        # else:
        return listCode,imgaligned

# if __name__ == "__main__":
#     c=ControlerClass()
#     a=c.connect_to_camera()
#     print(a)
